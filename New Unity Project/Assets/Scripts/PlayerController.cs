﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10;
        float ypo = Camera.main.ScreenToWorldPoint(mousePos).y;
        float xpo = Camera.main.ScreenToWorldPoint(mousePos).x;
        float AngleRad = Mathf.Atan2(ypo - transform.position.y, xpo - transform.position.x);
        float AngleDeg = (180 / Mathf.PI) * AngleRad;
        this.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
    }
}
